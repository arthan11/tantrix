import math
from PIL import Image, ImageDraw
from aggdraw import Draw, Brush, Pen
from time import sleep


back_color = (255, 0, 255, 0)
pen_color = 'black'
brush_color = 'white'
a = 180


def hexagon_generator(edge_length, offset):
    """Generator for coordinates in a hexagon."""
    x, y = offset
    for angle in range(0, 360, 60):
        x += math.cos(math.radians(angle)) * edge_length
        y += math.sin(math.radians(angle)) * edge_length
        yield x
        yield y

def draw_small_arc(x, y, r, a_start, a_stop):
    r += a / 3
    draw.pieslice((x-r, y-r, x+r, y+r), a_start, a_stop, Pen(pen_color), Brush(brush_color))
    r -= a / 3
    draw.pieslice((x-r, y-r, x+r, y+r), a_start, a_stop, Pen(pen_color), Brush(pen_color))

def small_arc(nr):
    if nr == 1:
        draw_small_arc(a * 1.5, 0, a/3, 180, 300)
    elif nr == 2:
        draw_small_arc(w, h/2, a/3, 120, 240)
    elif nr == 3:
        draw_small_arc(a * 1.5, h, a/3, 60, 180)
    elif nr == 4:
        draw_small_arc(a * 0.5, h, a/3, 0, 120)
    elif nr == 5:
        draw_small_arc(0, h/2, a/3, -60, 60)
    elif nr == 6:
        draw_small_arc(a * 0.5, 0, a/3, 240, 360)
        
def draw_vert_line():
    x = 0.5 * a + a / 3 
    draw.line((x, 0, x, h), Pen(pen_color))
    x = 0.5 * a + a / 3 * 2
    draw.line((x, 0, x, h), Pen(pen_color))

def draw_big_arc(x, y, r, a_start, a_stop):
    draw.arc((x-r, y-r, x+r, y+r), a_start, a_stop, Pen(pen_color))
    r += a / 3
    draw.arc((x-r, y-r, x+r, y+r), a_start, a_stop, Pen(pen_color))
    
if __name__ == '__main__':
    w = 2 * a
    h = int(math.ceil(a * math.sqrt(3)))
    x = a / 2

    # template 1

    image = Image.new('RGBA', (w, h), back_color)
    draw = Draw(image)
    #draw = ImageDraw.Draw(image)
    hexagon = hexagon_generator(a, offset=(x, 0))
    draw.polygon(list(hexagon), Pen(pen_color), Brush(pen_color))
    #draw.polygon(list(hexagon), outline='black', fill='red')

    small_arc(2)
    small_arc(4)
    small_arc(6)


    #draw_vert_line()
    
    #draw_big_arc(a, -h/2, a+a/3, 240, 300)
    #draw_big_arc(a, h*1.5, a+a/3, 60, 120)
    
    draw.flush()

    #ImageDraw.floodfill(image, (10, 10), (0, 0, 0))
    
    image.save('test1.png')