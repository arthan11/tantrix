from PIL import Image, ImageDraw

TILES = [
    [1,	37,	'YYBRBR' ],
    [2,	33,	'RRBYYB' ],
    [3,	29,	'BBYYRR' ],
    [4,	36,	'BRYBYR' ],
    [5,	32,	'BBRYYR' ],
    [6,	34,	'YBRYRB' ],
    [7,	42,	'BBYRYR' ],
    [8,	41,	'BBRYRY' ],
    [9,	35,	'RBYRYB' ],
    [10,	38,	'YYRBRB' ],
    [11,	39,	'RRBYBY' ],
    [12,	40,	'RRYBYB' ],
    [13,	31,	'BBYRRY' ],
    [14,	30,	'BBRRYY' ],
    [15,	5,	'GGRYYR' ],
    [16,	3,	'GGYRRY' ],
    [17,	10,	'YYGRGR' ],
    [18,	9,	'YYRGRG' ],
    [19,	13,	'RRGYGY' ],
    [20,	14,	'RRYGYG' ],
    [21,	1,	'GGRRYY' ],
    [22,	4,	'RRGYYG' ],
    [23,	2,	'GGYYRR' ],
    [24,	47,	'GGBRRB' ],
    [25,	43,	'BBGGRR' ],
    [26,	45,	'BBGRRG' ],
    [27,	53,	'RRBGBG' ],
    [28,	44,	'BBRRGG' ],
    [29,	54,	'RRGBGB' ],
    [30,	46,	'BBRGGR' ],
    [31,	11,	'GGRYRY' ],
    [32,	7,	'GRYGYR' ],
    [33,	12,	'GGYRYR' ],
    [34,	8,	'RGYRYG' ],
    [35,	6,	'YGRYRG' ],
    [36,	52,	'GGRBRB' ],
    [37,	56,	'BBGRGR' ],
    [38,	55,	'BBRGRG' ],
    [39,	48,	'GBRGRB' ],
    [40,	50,	'BGRBRG' ],
    [41,	51,	'GGBRBR' ],
    [42,	49,	'RBGRGB' ],
    [43,	15,	'BBYYGG' ],
    [44,	20,	'YBGYGB' ],
    [45,	16,	'BBGGYY' ],
    [46,	25,	'GGBYBY' ],
    [47,	17,	'BBYGGY' ],
    [48,	19,	'GGBYYB' ],
    [49,	18,	'BBGYYG' ],
    [50,	22,	'BGYBYG' ],
    [51,	21,	'GBYGYB' ],
    [52,	26,	'GGYBYB' ],
    [53,	24,	'YYGBGB' ],
    [54,	23,	'YYBGBG' ],
    [55,	27,	'BBGYGY' ],
    [56,	28,	'BBYGYG' ],
]

COLORS = {
    'R': (255, 0, 0),
    'G': (0, 255, 0),
    'B': (0, 0, 255),
    'Y': (255, 255, 0),
}

POINTS = (
    (310, 80),
    (310, 230),
    (180, 310),
    (50, 230),
    (50, 80),
    (180, 3),
)



for tile in TILES:
    tile_color = tile[2]
    print tile,
    template = 0

    if tile_color[0] == tile_color[1] and tile_color[2] == tile_color[3] and tile_color[4] == tile_color[5]:
        template = 1
    if tile_color[0] == tile_color[1] and tile_color[2] == tile_color[4] and tile_color[3] == tile_color[5]:
        template = 2
    if tile_color[0] == tile_color[1] and tile_color[2] == tile_color[5] and tile_color[3] == tile_color[4]:
        template = 3
    if tile_color[0] == tile_color[3] and tile_color[1] == tile_color[5] and tile_color[2] == tile_color[4]:
        tile_color = tile_color[-2:] + tile_color[:4]
        tile[2] = tile_color
        template = 4



    if template > 0:
        print ' .. ok'
        pic = Image.open('templates/{}.png'.format(template))
        for i, color in enumerate(tile[2]):
            point = pic.getpixel(POINTS[i])
            if point[0] != COLORS[color][0] or point[1] != COLORS[color][1] or point[2] != COLORS[color][2]:
                ImageDraw.floodfill(pic, POINTS[i], COLORS[color])
        pic.save("tiles/{}.png".format(tile[0]), "PNG", dpi=(300.0, 300.0))
    else:
        print ' .. fail!'
